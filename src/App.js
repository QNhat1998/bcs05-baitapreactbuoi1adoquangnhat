import logo from './logo.svg';
import './App.css';
import HeaderBT from './component/HeaderBT'
import Body from './component/Body'
import Footer from './component/Footer'

function App() {
  return (
    <div className="App">
      <HeaderBT></HeaderBT>
      <Body></Body>
      <Footer></Footer>
    </div>
  );
}

export default App;
