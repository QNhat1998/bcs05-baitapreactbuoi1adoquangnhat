import React, { Component } from 'react'
import Banner from './Banner'
import ItemBody from './ItemBody'

export default class Body extends Component {
  render() {
    return (
        <div className="container p-5">
            <Banner></Banner>
            <ItemBody></ItemBody>
        </div>
    )
  }
}
